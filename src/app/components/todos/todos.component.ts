import { Component, OnInit } from '@angular/core';
import {TodoService} from '../../services/todo.service';
import { Todo } from '../../models/Todo';
@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  arrayOfObjectsTodo: Todo[];
  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
    this.todoService.getTodosHttp().subscribe(todoItems=>{
      this.arrayOfObjectsTodo = todoItems;
    });
  }
  deleteTodoB(todo: Todo){
    // Remove from UI
    this.arrayOfObjectsTodo = this.arrayOfObjectsTodo.filter(t=>t.id!==todo.id);
    //Remove from server
    this.todoService.deleteTodo(todo).subscribe();
  }
  addTodoB(todo: Todo) {
    this.todoService.addTodo(todo).subscribe(t=>{
      this.arrayOfObjectsTodo.push(t);
    })
  }
}
