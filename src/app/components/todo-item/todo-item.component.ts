import { Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { Todo } from '../../models/Todo';
import { TodoService } from '../../services/todo.service';
@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {

  @Input() passedTodoFromTodosComponent: Todo; 
  @Output() deleteTodoF: EventEmitter<Todo> = new EventEmitter(); 

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
  }
  //set Dynamic Classes
  setClasses() {
    let classes={
      'todo-div': true,
      'is-complete': this.passedTodoFromTodosComponent.completed
    }
    return classes;
  }

  onToggle(todoItem) {
    // Toggle in UI
    todoItem.completed=!todoItem.completed;
    // Toggle on server
    this.todoService.toggleCompleted(todoItem).subscribe(todo=>
      console.log(todo));
  }

  onDelete(todoItem) {
    this.deleteTodoF.emit(todoItem);
  }

}
