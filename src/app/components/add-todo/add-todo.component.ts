import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {
  @Output() addTodoF: EventEmitter<any> = new EventEmitter();

  inputValue:String;
  constructor() { }

  ngOnInit(): void {
  }
  onSubmit() {
    const todo = {
      title: this.inputValue,
      completed: false
    }
    this.addTodoF.emit(todo);
  }
}
